package com.springmybatis.learn.mapper;

import com.springmybatis.learn.bean.Person;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PersonMapper {
    @Insert("insert into user(id,name,pwd)values(#{id},#{name},#{pwd})")
    int insert(Person person);
    @Delete("delete from user where id=#{id}")
    int deleteByPrimaryKey(Integer id);
    @Update("update user set name=#{name},pwd=#{pwd} where id=#{id}")
    int updateByPrimaryKey(Integer id);
    @Select("select id,name,pwd from user where id=#{id}")
    Person selectByPrimaryKey(Integer id);
    @Select("select id,name,pwd from user")
    List<Person> selectAllPerson();
}

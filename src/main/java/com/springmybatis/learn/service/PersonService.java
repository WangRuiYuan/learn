package com.springmybatis.learn.service;

import com.springmybatis.learn.bean.Person;

import java.util.List;

public interface PersonService {
    int insertPerson(Person person);
    int deleteByPersonId(Integer id);
    int updateByPersonId(Integer id);
    Person selectByPersonId(Integer id);
    List<Person> selectAllPerson();
}

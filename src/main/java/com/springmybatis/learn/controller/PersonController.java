package com.springmybatis.learn.controller;

import com.springmybatis.learn.bean.Person;
import com.springmybatis.learn.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonController {

    @Autowired
    private PersonService personService;
    @RequestMapping(value="/add")
    @ResponseBody
    public String student(){
        Person person=new Person();
        person.setId(24124345);
        person.setName("鹅鹅鹅");
        person.setPwd(3563434);
        int result=personService.insertPerson(person);
        System.out.println("插入的结果是"+result);
        return  result+"";
    }
    @RequestMapping(value="/findAll")
    @ResponseBody
    public String findAll(){
        List<Person> people=personService.selectAllPerson();
        people.stream().forEach(System.out::println);
        return people.toString()+"";
    }
    @RequestMapping(value = "/hello", method= RequestMethod.GET)
    public String hello() {
        return "Hello !";
    }

}
